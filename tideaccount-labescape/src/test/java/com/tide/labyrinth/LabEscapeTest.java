package com.tide.labyrinth;
import static org.junit.Assert.assertEquals;
import static com.tide.rest.adapter.LabyrinthDeSerialiser.splitLabyrinthToCharArray;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.tide.labyrinth.LabEscape;
import com.tide.labyrinth.NoEscapeException;

public class LabEscapeTest {
	
	@Rule
	public ExpectedException excpetion = ExpectedException.none();
	  
	

	@Test
	public void testLabGivenExample() throws NoEscapeException{
		String labyrinthString = 	"OOOOOOOOOO" + "\n" +
									"O    O   O" + "\n" +
									"O OO O O O" + "\n" +
									"O  O O O O" + "\n" +
									"O OO   O  " + "\n" +
									"O OOOOOOOO" + "\n" +
									"O        O" + "\n" +
									"OOOOOOOOOO";
		String labyrinthEscapeString = 	"OOOOOOOOOO" +
										"O����O���O" +
										"O�OO�O�O�O" +
										"O� O�O�O�O" +
										"O OO���O��" +
										"O OOOOOOOO" +
										"O        O" +
										"OOOOOOOOOO";
		
		char[][] labyrinth = splitLabyrinthToCharArray(labyrinthString);
		LabEscape.drawPathForEscape(labyrinth, 3, 1);
		assertEquals(labyrinthEscapeString, charArrayToString(labyrinth));
		
	}
	@Test
	public void testLabPath2() throws NoEscapeException{
		String labyrinthString = 	"OOOOOOOOOO" + "\n" +
									"O    O   O" + "\n" +
									"O OO O O O" + "\n" +
									"O  O O O O" + "\n" +
									"O OO   O O" + "\n" +
									"O OOOOOOOO" + "\n" +
									"O        O" + "\n" +
									"OOOOOOO OO";
		String labyrinthEscapeString = 	"OOOOOOOOOO" +
										"O    O   O" +
										"O OO O O O" +
										"O� O O O O" +
										"O�OO   O O" +
										"O�OOOOOOOO" +
										"O������� O" +
										"OOOOOOO�OO";
		char[][] labyrinth = splitLabyrinthToCharArray(labyrinthString);
		LabEscape.drawPathForEscape(labyrinth, 3, 1);
		assertEquals(labyrinthEscapeString, charArrayToString(labyrinth));
		
	}
	
	@Test
	public void testLabPath3() throws NoEscapeException{
		String labyrinthString = 	"OOOOOOOOOO" + "\n" +
									"O    O   O" + "\n" +
									"O OO O O O" + "\n" +
									"   O O O O" + "\n" +
									"O OO   O O" + "\n" +
									"O OOOO OOO" + "\n" +
									"O OO     O" + "\n" +
									"OOOOOOOOOO" + "\n" ;
		
		String labyrinthEscapeString = 	"OOOOOOOOOO" + 
										"O����O   O" + 
										"O�OO�O O O" + 
										"�� O�O O O" + 
										"O OO���O O" + 
										"O OOOO�OOO" + 
										"O OO ��  O" + 
										"OOOOOOOOOO";
		char[][] labyrinth = splitLabyrinthToCharArray(labyrinthString);
		LabEscape.drawPathForEscape(labyrinth, 6, 5);
		assertEquals(labyrinthEscapeString, charArrayToString(labyrinth));
		
	}
	
	@Test
	public void testLabPath4NoEscape() throws NoEscapeException{
		String labyrinthString = 	"OOOO" + "\n" +
									"O  O" + "\n" +
									"O  O" + "\n" +
									"OOOO"  ;
		char[][] labyrinth = splitLabyrinthToCharArray(labyrinthString);
		excpetion.expect(NoEscapeException.class);;
		LabEscape.drawPathForEscape(labyrinth, 2, 1);
		
	}
	@Test
	public void testLabPath5() throws NoEscapeException{
		String labyrinthString = 	"O OO" + "\n" +
									"O  O" + "\n" +
									"O  O" + "\n" +
									"OOOO"  ;
		String labyrinthEscapeString =	"O�OO" +
										"O� O" +
										"O� O" +
										"OOOO";
		char[][] labyrinth = splitLabyrinthToCharArray(labyrinthString);
	
		LabEscape.drawPathForEscape(labyrinth, 2, 1);
		assertEquals(labyrinthEscapeString, charArrayToString(labyrinth));
		
	}
	
	@Test
	public void testLabPath6NoEscape() throws NoEscapeException{
		String labyrinthString = 	"OOOO" + "\n" +
									"O  O" + "\n" +
									"OOOO"  ;
		char[][] labyrinth = splitLabyrinthToCharArray(labyrinthString);
		excpetion.expect(NoEscapeException.class);;
		LabEscape.drawPathForEscape(labyrinth, 1, 1);
		
	}
	@Test
	public void testLabPath7() throws NoEscapeException{
		String labyrinthString = 	"OOOOOOOOOOOOO" + "\n"  +
									"O    O   OOOO" + "\n"  +
									"O OO O O OOOO" + "\n"  +
									"O  O O O OOOO" + "\n"  +
									"O OO   O    O" + "\n"  +
									"O OOOOOOOOO O" + "\n"  +
									"O    OOOO   O" + "\n"  +
									"OOOOOOOOOO OO" + "\n"  +
									"OOOOOOO OO OO" ;
		String labyrinthEscapeString =	"OOOOOOOOOOOOO" +
										"O����O���OOOO" +
										"O�OO�O�O�OOOO" +
										"O� O�O�O�OOOO" +
										"O�OO���O����O" +
										"O�OOOOOOOOO�O" +
										"O����OOOO ��O" +
										"OOOOOOOOOO�OO" +
										"OOOOOOO OO�OO";
		char[][] labyrinth = splitLabyrinthToCharArray(labyrinthString);
		char PATH = '�';
		System.out.println((int) PATH );
		LabEscape.drawPathForEscape(labyrinth, 6, 4);
		assertEquals(labyrinthEscapeString, charArrayToString(labyrinth));
		
	}
	
	
	private String charArrayToString(char[][] escape) {
		StringBuilder strBuilder = new StringBuilder();
		for(int i=0; i<escape.length; i++){
			for(int j=0; j<escape[i].length; j++){
				strBuilder.append(escape[i][j]);
			}
		}
		
		return strBuilder.toString();
	}
}

package com.tide.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
@RunWith(SpringRunner.class)
@WebMvcTest(LabyrinthController.class)
public class LabyrinthControllerTest {
	 
	@Autowired
	private MockMvc mockMvc;

	@Test
    public void greetingShouldReturnMessageFromService() throws Exception {
		
        
        String request = "{\"labyrinth\":\"OOOOOOOOOO\\nO    O   O\\nO OO O O O\\nO  O O O O\\nO OO   O  \\nO OOOOOOOO\\nO        O\\nOOOOOOOOOO\",  \"startX\": 3,   \"startY\": 1}";
        this.mockMvc.perform(post("/escape").contentType(MediaType.APPLICATION_JSON).content(request)).andExpect(status().isOk());
    }

	
	
}

package com.tide.rest;

public class LabyrinthResponse {
	private String labyrinthSolution;

	public String getLabyrinthSolution() {
		return labyrinthSolution;
	}

	public void setLabyrinthSolution(String labyrinthSolution) {
		this.labyrinthSolution = labyrinthSolution;
	}
}

package com.tide.rest.adapter;

public class LabyrinthSerialiser {

	public static String labyrinthToNewLineDelimitedString(char[][] escape) {
		StringBuilder strBuilder = new StringBuilder();
		for(int i=0; i<escape.length; i++){
			for(int j=0; j<escape[i].length; j++){
				strBuilder.append(escape[i][j]);
			}
			if(i < escape.length -1) strBuilder.append("\n");
		}
		
		return strBuilder.toString();
	}
}

package com.tide.rest.adapter;

public class LabyrinthDeSerialiser {

	public static char[][] splitLabyrinthToCharArray(String maze){
		String[] lines = maze.split("\n");
		char[][] labyrinth = new char[lines.length][lines[0].length()];
		for(int i = 0; i< lines.length ; i++){
			labyrinth[i] = lines[i].toCharArray();
		}
		return labyrinth;
	}
}

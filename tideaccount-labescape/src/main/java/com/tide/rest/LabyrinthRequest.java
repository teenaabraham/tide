package com.tide.rest;

public class LabyrinthRequest {

	private String labyrinth;
	private int startX;
	private int startY;
	
	
	public String getLabyrinth() {
		return labyrinth;
	}
	public void setLabyrinth(String labyrinth) {
		this.labyrinth = labyrinth;
	}
	public int getStartX() {
		return startX;
	}
	public void setStartX(int startX) {
		this.startX = startX;
	}
	public int getStartY() {
		return startY;
	}
	public void setStartY(int startY) {
		this.startY = startY;
	}
}

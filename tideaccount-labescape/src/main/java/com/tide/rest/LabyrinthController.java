package com.tide.rest;


import static com.tide.rest.adapter.LabyrinthDeSerialiser.splitLabyrinthToCharArray;
import static com.tide.rest.adapter.LabyrinthSerialiser.labyrinthToNewLineDelimitedString;


import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tide.labyrinth.LabEscape;
import com.tide.labyrinth.NoEscapeException;;

@RestController
public class LabyrinthController {
	
	
	@RequestMapping(value="/escape",method=RequestMethod.POST, produces="application/json; charset=UTF-8", consumes="application/json")
	public @ResponseBody LabyrinthResponse escape(@RequestBody LabyrinthRequest request) throws NoEscapeException{
		String escapeString = labyrinthToNewLineDelimitedString(LabEscape.drawPathForEscape(
				splitLabyrinthToCharArray(request.getLabyrinth()), request.getStartX(),
				request.getStartY()));
		
		LabyrinthResponse response = new LabyrinthResponse();
		response.setLabyrinthSolution(escapeString);
		return response;
	}
	
	
}

package com.tide.labyrinth;
/**
 * Please implement your solution here
 */
public class LabEscape {

    private static final char WALL = 'O';
    private static final char FREE = ' ';
    private static final char PATH = '�';  

    /**
     * @param labyrinth A labyrinth drawn on a matrix of characters. It's at least 4x4, can be a rectangle or a square.
     *                  Walkable areas are represented with a space character, walls are represented with a big O character.
     *                  The escape point is always on the border (see README)
     * @param startX    Starting row number for the escape. 0 based.
     * @param startY    Starting column number for the escape. 0 based.
     * @return          A char matrix with the same labyrinth and a path drawn from the starting point to the escape
     * @throws          NoEscapeException when no path exists to the outside, from the selected starting point
     */
    public static char[][] drawPathForEscape(char[][] labyrinth, int startX, int startY) throws NoEscapeException {

    	int length = labyrinth.length;
    	int width = labyrinth[0].length;
    	if(length > 3 && width > 3 
    			&& labyrinth[startX][startY] == FREE && hasExcapeRoute(labyrinth, startX, startY, length, width)){
    		 return labyrinth;
    	}
    	throw new NoEscapeException();
    	 		
       
    }
    
    private static boolean hasExcapeRoute(char[][] labyrinth, int x, int y, int length, int width) {
    	
    	labyrinth[x][y] = PATH;
    	if(x == 0 || y == 0 || x == length-1 || y == width-1){
    		return true;
    	}
    	if((labyrinth[x-1][y] == FREE && hasExcapeRoute(labyrinth, x-1, y, length, width))
    			|| (labyrinth[x+1][y] == FREE && hasExcapeRoute(labyrinth, x+1, y, length, width))
    			|| (labyrinth[x][y-1] == FREE && hasExcapeRoute(labyrinth, x, y-1, length, width))
    			|| (labyrinth[x][y+1] == FREE && hasExcapeRoute(labyrinth, x, y+1, length, width))){
    		return true;
    	}
    	labyrinth[x][y] = FREE;
    	return false;
    }
}
